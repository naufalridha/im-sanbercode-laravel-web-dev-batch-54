<?php
require_once("animal.php");
require_once("frog.php");
require_once("ape.php");

echo "<h2> Latihan OOP</h2>";

$sheep = new Animal("Shaun");
echo "Name: " . $sheep->hewan . "<br>";
echo "legs: " . $sheep->legs . "<br>";
echo "cold blooded: " . $sheep-> cold_blooded . "<br> <br>";

$kodok = new Frog("buduk");
echo "Name: " . $kodok->hewan . "<br>";
echo "legs: " . $kodok->legs . "<br>";
echo "cold blooded: " . $kodok-> cold_blooded . "<br>";
echo $kodok->jump() . "<br> <br>";

$sungokong = new Monyet("kera sakti");
echo "Name: " . $sungokong->hewan . "<br>";
echo "legs: " . $sungokong->legs . "<br>";
echo "cold blooded: " . $sungokong-> cold_blooded . "<br>";
echo $sungokong->yell();
?>